<?php

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1419899211.
 * Generated on 2014-12-30 01:26:51 by root
 */
class PropelMigration_1419899211
{

    public function preUp($manager)
    {
        // add the pre-migration code here
    }

    public function postUp($manager)
    {
        // add the post-migration code here
    }

    public function preDown($manager)
    {
        // add the pre-migration code here
    }

    public function postDown($manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `product` CHANGE `price` `price` DECIMAL;

ALTER TABLE `product`
    ADD `category_id` INTEGER AFTER `description`;

CREATE INDEX `product_FI_1` ON `product` (`category_id`);

ALTER TABLE `product` ADD CONSTRAINT `product_FK_1`
    FOREIGN KEY (`category_id`)
    REFERENCES `category` (`id`);

CREATE TABLE `category`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(100),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `category`;

ALTER TABLE `product` DROP FOREIGN KEY `product_FK_1`;

DROP INDEX `product_FI_1` ON `product`;

ALTER TABLE `product` CHANGE `price` `price` DECIMAL(10,2);

ALTER TABLE `product` DROP `category_id`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}
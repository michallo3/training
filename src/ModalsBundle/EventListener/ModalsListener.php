<?php
namespace ModalsBundle\EventListener;

use ModalsBundle\Service\ModalsManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ModalsListener{

    private $modalsManager;

    public function __construct(ModalsManager $modalsManager)
    {
        $this->modalsManager = $modalsManager;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        /** @var ModalsManager $modalsManager */
        $this->modalsManager->setRequest($request);
        $this->modalsManager->appendModalsContent($response);
        $event->setResponse($response);
    }
}
<?php

namespace ModalsBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ModalsExtension extends Extension{

    /**
     * Loads a specific configuration.
     *
     * @param array $config An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     *
     * @api
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $processedConfig = $this->processConfiguration(new Configuration(), $config);


        $this->processModals($processedConfig, $container);
    }

    private function processModals(array $config, ContainerBuilder $container)
    {
        $modalsManagerDefinition = $container->getDefinition('modals.manager');

        foreach ($config as $modalConfiguration) {
            $serviceId = $modalConfiguration['service'];
            $modalService = $container->getDefinition($serviceId);
            $modalService->addMethodCall('setAllowedRoutes',[$modalConfiguration['allowed_routes']]);
            $modalService->addMethodCall('setExcludedRoutes',[$modalConfiguration['excluded_routes']]);

            $modalServiceRef = new Reference($serviceId);
            $modalsManagerDefinition->addMethodCall('addModal',[$modalServiceRef]);
        }

    }
}
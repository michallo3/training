<?php

namespace ModalsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $modalsNode = $treeBuilder->root('modals');
        $modalsNode
            ->useAttributeAsKey('name')
                ->prototype('array')
                    ->children()
                        ->scalarNode('service')
                            ->isRequired()
                        ->end()
                    ->arrayNode('allowed_routes')
                        ->prototype('scalar')
                        ->end()
                    ->end()
                    ->arrayNode('excluded_routes')
                        ->prototype('scalar')
                        ->end()
                    ->end()
                ->end()
            ->end();


        return $treeBuilder;

    }

}
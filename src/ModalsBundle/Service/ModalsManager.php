<?php

namespace ModalsBundle\Service;

use ModalsBundle\Exception\MissingMandatoryParametersException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ModalsManager
{
    /**
     * @var ModalAbstract[]
     */
    private $modals;

    /**
     * @var Request
     */
    private $request;

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function addModal(ModalAbstract $modal)
    {
        $this->modals[] = $modal;
    }

    public function appendModalsContent(Response $response)
    {
        if (null === $this->request) {
            throw new MissingMandatoryParametersException('You have to set the request!');
        }

        if (true === empty($this->modals)) {
            return $response;
        }

        foreach ($this->modals as $modal) {
            $modalsContent = '';
            if ($modal->isSupportedRoute($this->request->get('_route')) && $modal->doesMeetRequirements()) {
                $modalsContent .= $modal->getContent();
            }
        }
        $this->putModalsContent($response, $modalsContent);
    }

    private function putModalsContent(Response $response, $modalsContent)
    {
        $content = $response->getContent();
        $content = str_replace('<!--MODALS_CONTENT-->', $modalsContent, $content);
        $response->setContent($content);
    }

}
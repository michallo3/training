<?php

namespace ModalsBundle\Service;

use ModalsBundle\Exception\InvalidAttributesException;
use Symfony\Bundle\TwigBundle\TwigEngine;

abstract class ModalAbstract
{

    /**
     * Routy na których modal ma się pojawić
     * @var string[]
     */
    protected $allowedRoutes;

    /**
     * Routy, na których modal ma się nie pojawić
     * @param $route
     * @return mixed
     */
    protected $excludedRoutes;

    /**
     * @var TwigEngine
     */
    protected $templating;

    public function __construct(TwigEngine $templating)
    {
        $this->templating = $templating;
    }

    /**
     * Sprawdzenie czy modal obsługuje podaną route
     * @param $route
     * @return mixed
     */
    public function isSupportedRoute($route)
    {
        if (false === empty($this->excludedRoutes) && false === empty($this->allowedRoutes)) {
            throw new InvalidAttributesException('You cannot set excludedRoutes and allowedRoutes at once in modal');
        }

        if (true === empty($this->allowedRoutes) && true === empty($this->excludedRoutes)) {
            return true;
        }

        if (true === empty($this->allowedRoutes) && false === in_array($route, $this->excludedRoutes)) {
            return true;
        }

        if (true === in_array($route, $this->allowedRoutes)) {
            return true;
        }

        return false;
    }

    /**
     * @param string[] $routes
     */
    public function setAllowedRoutes($routes)
    {
        $this->allowedRoutes = $routes;
    }

    /**
     * @param string[] $routes
     */
    public function setExcludedRoutes($routes)
    {
        $this->excludedRoutes = $routes;
    }

    abstract public function getContent();

    /**
     * Metoda warunkująca dodanie contentu modala
     * @return bool
     */
    abstract public function doesMeetRequirements();
} 
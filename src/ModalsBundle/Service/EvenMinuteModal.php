<?php

namespace ModalsBundle\Service;

class EvenMinuteModal extends ModalAbstract
{

    public function getContent()
    {
        return $this->templating->render('ModalsBundle::evenMinuteModal.html.twig', ['name' => 'Jest parzysta minuta !']);
    }

    /**
     * Metoda warunkująca dodanie contentu modala
     *
     * @return bool
     */
    public function doesMeetRequirements()
    {
        return $this->isEvenMinute();
    }

    private function isEvenMinute()
    {
        $time    = new \DateTime();
        $minutes = $time->format('i');

        return 0 === $minutes % 2;
    }
}
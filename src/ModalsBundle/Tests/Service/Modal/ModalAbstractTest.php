<?php

namespace ModalsBundle\Tests\Modal;

use ModalsBundle\Modal\MockModal;
use \Mockery as m;

class ModalAbstractTest extends \PHPUnit_Framework_TestCase
{
    public function testIsSupportedRouteByAllowed()
    {
        $twig = $this->getTwigMock();
        $modal = new MockModal($twig);

        $allowedRoutes = [
            'route_1',
            'route_2'
        ];

        $modal->setAllowedRoutes($allowedRoutes);
        $modal->setExcludedRoutes([]);
        $this->assertTrue($modal->isSupportedRoute('route_1'));
        $this->assertFalse($modal->isSupportedRoute('route_3'));
    }

    public function testIsSupportedRouteByExcluded()
    {
        $twig = $this->getTwigMock();
        $modal = new MockModal($twig);

        $excludedRoutes = [
            'route_1',
            'route_2'
        ];

        $modal->setExcludedRoutes($excludedRoutes);
        $modal->setAllowedRoutes([]);
        $this->assertFalse($modal->isSupportedRoute('route_1'));
        $this->assertTrue($modal->isSupportedRoute('route_3'));
    }

    private function getTwigMock(){
        return m::mock('\Symfony\Bundle\TwigBundle\TwigEngine');
    }

    /**
     * @expectedException ModalsBundle\Exception\InvalidAttributesException
     */
    public function testInvalidAttributesException(){
        $twig = $this->getTwigMock();
        $modal = new MockModal($twig);

        $modal->setAllowedRoutes(['route_1']);
        $modal->setExcludedRoutes(['route_2']);

        $modal->isSupportedRoute('route_1');
    }
}
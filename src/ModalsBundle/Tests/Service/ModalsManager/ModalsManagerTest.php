<?php

use \Mockery as m;
use ModalsBundle\Service\ModalsManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ModalsManagerTest extends \PHPUnit_Framework_TestCase
{

    public function testAppendModalsContent()
    {
        $request = new Request();
        $response = new Response($this->getResponseContent());

        $modalsManager = new ModalsManager();
        $modalsManager->setRequest($request);
        $modalsManager->addModal($this->getEvenMinuteModalMock());

        $modalsManager->appendModalsContent($response);
        $this->assertGreaterThan(0,strpos($response->getContent(),'<div>TRESC_MODALA</div>'));
    }

    /**
     * @return string
     */
    private function getResponseContent()
    {
        return '<body>
                <!--MODALS_CONTENT-->
                </body>';
    }

    private function getEvenMinuteModalMock(){
        $m = m::mock('\ModalsBundle\Service\EvenMinuteModal');
        $m->shouldReceive('doesMeetRequirements')->andReturn(true);
        $m->shouldReceive('getContent')->andReturn('<div>TRESC_MODALA</div>');
        $m->shouldReceive('isSupportedRoute')->andReturn(true);
        return $m;
    }
} 
<?php
namespace ModalsBundle\Modal;

use ModalsBundle\Service\ModalAbstract;

/**
 * Klasa na potrzeby testowania abstrakcyjnych metod
 * Class MockModal
 */
class MockModal extends ModalAbstract{

    public function getContent(){}

    /**
     * Metoda warunkująca dodanie contentu modala
     * @return bool
     */
    public function doesMeetRequirements(){}
}